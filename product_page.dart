import 'package:flutter/material.dart';
import 'package:flutter_application_1/pages/login_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        scaffoldBackgroundColor:
            Colors.pink, // Set the background color to pink
      ),
      home: ProductScreen(),
      routes: {
        '/cart': (context) => CartPage(
              cartItems: [],
            ),
      },
    );
  }
}

class ProductScreen extends StatefulWidget {
  @override
  _ProductScreenState createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  List<ProductModel> cartItems = [];

  void addToCart(ProductModel product) {
    setState(() {
      cartItems.add(product);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Shoe Market'),
        actions: [
          IconButton(
            icon: Icon(Icons.person),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ProfileScreen()),
              );
            },
          ),
          IconButton(
            icon: Icon(Icons.logout),
            onPressed: () {
              // Perform logout action
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => LoginPage()),
              );
            },
          ),
        ],
      ),
      backgroundColor:
          Colors.pink, // Set the background color for the product screen
      body: GridView.count(
        crossAxisCount: 2,
        padding: EdgeInsets.all(20.0),
        children: [
          ProductItem(
            imageUrl: 'assets/images/image1.jpg',
            name: 'Running Shoes',
            price: '\$99.99',
            onPressed: () {
              addToCart(ProductModel(
                imageUrl: 'assets/images/image1.jpg',
                name: 'Running Shoes',
                price: '\$99.99',
              ));
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CartPage(cartItems: cartItems),
                ),
              );
            },
          ),
          ProductItem(
            imageUrl: 'assets/images/image2.jpg',
            name: 'Sneakers',
            price: '\$79.99',
            onPressed: () {
              addToCart(ProductModel(
                imageUrl: 'assets/images/image2.jpg',
                name: 'Sneakers',
                price: '\$79.99',
              ));
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CartPage(cartItems: cartItems),
                ),
              );
            },
          ),
          ProductItem(
            imageUrl: 'assets/images/image3.jpg',
            name: 'Flip Flops',
            price: '\$29.99',
            onPressed: () {
              addToCart(ProductModel(
                imageUrl: 'assets/images/image3.jpg',
                name: 'Flip Flops',
                price: '\$29.99',
              ));
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CartPage(cartItems: cartItems),
                ),
              );
            },
          ),
          ProductItem(
            imageUrl: 'assets/images/image4.jpg',
            name: 'Flip Flops',
            price: '\$29.99',
            onPressed: () {
              addToCart(ProductModel(
                imageUrl: 'assets/images/image4.jpg',
                name: 'Flip Flops',
                price: '\$29.99',
              ));
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CartPage(cartItems: cartItems),
                ),
              );
            },
          ),
          ProductItem(
            imageUrl: 'assets/images/image5.jpg',
            name: 'Flip Flops',
            price: '\$29.99',
            onPressed: () {
              addToCart(ProductModel(
                imageUrl: 'assets/images/image5.jpg',
                name: 'Flip Flops',
                price: '\$29.99',
              ));
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CartPage(cartItems: cartItems),
                ),
              );
            },
          ),
          ProductItem(
            imageUrl: 'assets/images/image6.jpg',
            name: 'Flip Flops',
            price: '\$29.99',
            onPressed: () {
              addToCart(ProductModel(
                imageUrl: 'assets/images/image6.jpg',
                name: 'Flip Flops',
                price: '\$29.99',
              ));
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CartPage(cartItems: cartItems),
                ),
              );
            },
          ),
          ProductItem(
            imageUrl: 'assets/images/image7.jpg',
            name: 'Flip Flops',
            price: '\$29.99',
            onPressed: () {
              addToCart(ProductModel(
                imageUrl: 'assets/images/image7.jpg',
                name: 'Flip Flops',
                price: '\$29.99',
              ));
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CartPage(cartItems: cartItems),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
// Add more ProductItem widgets for other products

class ProductItem extends StatelessWidget {
  final String imageUrl;
  final String name;
  final String price;
  final VoidCallback onPressed;

  const ProductItem({
    required this.imageUrl,
    required this.name,
    required this.price,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Column(
        children: [
          Image.asset(
            imageUrl,
            width: 100,
            height: 100,
          ),
          Text(
            name,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(price),
          ElevatedButton(
            onPressed: onPressed,
            child: Text('Buy'),
          ),
        ],
      ),
    );
  }
}

class CartPage extends StatelessWidget {
  final List<ProductModel> cartItems;

  const CartPage({required this.cartItems});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cart'),
      ),
      backgroundColor: Colors.pink, // Set the background color to pink
      body: ListView.builder(
        itemCount: cartItems.length,
        itemBuilder: (context, index) {
          final item = cartItems[index];
          return ListTile(
            leading: Image.asset(
              item.imageUrl,
              width: 50,
              height: 50,
            ),
            title: Text(item.name),
            subtitle: Text(item.price),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pop(context);
        },
        child: Icon(Icons.arrow_back),
      ),
    );
  }
}

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
      ),
      backgroundColor: Colors.pink, // Set the background color to pink
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 80,
              backgroundImage: AssetImage('assets/images/profile_image.jpg'),
            ),
            SizedBox(height: 20),
            Text(
              'Gemma Rose Bermudez',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 10),
            Text(
              'Software Engineer',
              style: TextStyle(
                fontSize: 16,
                color: Colors.grey[600],
              ),
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                // Perform edit profile action
              },
              child: Text('Edit Profile'),
            ),
          ],
        ),
      ),
    );
  }
}

class ProductModel {
  final String imageUrl;
  final String name;
  final String price;

  ProductModel({
    required this.imageUrl,
    required this.name,
    required this.price,
  });
}
